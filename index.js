const os = require('os')
const exec = require('child_process').exec
module.exports = () => {
    return new Promise((resolve,reject) => {
        var networkControllers = os.networkInterfaces()
        exec('ifconfig',function(err,data){
            var interfaceInfoRaw = data.toString()
            var interfaceInfoRawLines = interfaceInfoRaw.split('\n')
            var interfaces = {}
            var controllerName
            interfaceInfoRawLines.forEach(function(line){
                var rawLineParts = line.trim().split(' ')
                var lineParts = []
                rawLineParts.forEach(function(part){
                    if(part){
                        part = part.trim()
                        lineParts.push(parseFloat(part) || part)
                    }
                })
                if(lineParts.length > 0){
                    switch(lineParts[0]){
                        case'inet':case'inet6':case'loop':case'ether':
                        break;
                        case'TX':
                            if(lineParts[1] === 'packets'){
                                interfaces[controllerName].txBytes = lineParts[2]
                                interfaces[controllerName].txTransfer = lineParts[5].replace('(','') + ' ' + lineParts[6].replace(')','')
                            }else{
                                interfaces[controllerName].txError = lineParts[2]
                                interfaces[controllerName].txDropped = lineParts[4]
                                interfaces[controllerName].txOverruns = lineParts[6]
                                interfaces[controllerName].txCarrier = lineParts[8]
                                interfaces[controllerName].txCollisions = lineParts[10]
                            }
                        break;
                        case'RX':
                            if(lineParts[1] === 'packets'){
                                interfaces[controllerName].rxBytes = lineParts[2]
                                interfaces[controllerName].rxTransfer = lineParts[5].replace('(','') + ' ' + lineParts[6].replace(')','')
                            }else{
                                interfaces[controllerName].rxError = lineParts[2]
                                interfaces[controllerName].rxDropped = lineParts[4]
                                interfaces[controllerName].rxOverruns = lineParts[6]
                                interfaces[controllerName].rxFrame = lineParts[8]
                            }
                        break;
                        default:
                            controllerName = lineParts[0].replace(':','')
                            interfaces[controllerName] = {
                                connections: networkControllers[controllerName]
                            }
                        break;
                    }
                }
            })
            resolve(interfaces)
        })
    })
}
