# shinobi-ifconfig

> Information from ifconfig, Parsed Network Information

**Install**

```
npm install shinobi-ifconfig
```

**Use**

```
const ifconfig = require('shinobi-ifconfig')

console.log(ifconfig())
```

**Sample Output**

```
{
   "docker0": {
      "rxBytes": "0",
      "rxTransfer": "0.0 B",
      "rxError": "0",
      "rxDropped": "0",
      "rxOverruns": "0",
      "rxFrame": "0",
      "txBytes": "0",
      "txTransfer": "0.0 B",
      "txError": "0",
      "txDropped": "0",
      "txOverruns": "0",
      "txCarrier": "0",
      "txCollisions": "0"
   },
   "eth0": {
      "connections": [
         {
            "address": "111.111.111.111",
            "netmask": "255.255.255.0",
            "family": "IPv4",
            "mac": "00:00:00:00:00:00",
            "internal": false,
            "cidr": "111.111.111.111/24"
         },
         {
            "address": "9999:999:9999:9999:999:9999:9999:9999",
            "netmask": "ffff:ffff:ffff:ffff::",
            "family": "IPv6",
            "mac": "00:00:00:00:00:00",
            "internal": false,
            "cidr": "9999:999:9999:9999:999:9999:9999:9999/64",
            "scopeid": 0
         },
         {
            "address": "9999:999:9999:9999:999:9999:9999:9999",
            "netmask": "ffff:ffff:ffff:ffff::",
            "family": "IPv6",
            "mac": "00:00:00:00:00:00",
            "internal": false,
            "cidr": "9999:999:9999:9999:999:9999:9999:9999/64",
            "scopeid": 3
         }
      ],
      "rxBytes": 52397048,
      "rxTransfer": "37.9 GB",
      "rxError": "0",
      "rxDropped": 175429,
      "rxOverruns": "0",
      "rxFrame": "0",
      "txBytes": 31595327,
      "txTransfer": "69.0 GB",
      "txError": "0",
      "txDropped": "0",
      "txOverruns": "0",
      "txCarrier": "0",
      "txCollisions": "0"
   },
   "device": {},
   "lo": {
      "connections": [
         {
            "address": "127.0.0.1",
            "netmask": "255.0.0.0",
            "family": "IPv4",
            "mac": "00:00:00:00:00:00",
            "internal": true,
            "cidr": "127.0.0.1/8"
         },
         {
            "address": "::1",
            "netmask": "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff",
            "family": "IPv6",
            "mac": "00:00:00:00:00:00",
            "internal": true,
            "cidr": "::1/128",
            "scopeid": 0
         }
      ],
      "rxBytes": 3321723,
      "rxTransfer": "712.3 MB",
      "rxError": "0",
      "rxDropped": "0",
      "rxOverruns": "0",
      "rxFrame": "0",
      "txBytes": 3321723,
      "txTransfer": "712.3 MB",
      "txError": "0",
      "txDropped": "0",
      "txOverruns": "0",
      "txCarrier": "0",
      "txCollisions": "0"
   },
   "rndis0": {
      "rxBytes": "0",
      "rxTransfer": "0.0 B",
      "rxError": "0",
      "rxDropped": "0",
      "rxOverruns": "0",
      "rxFrame": "0",
      "txBytes": "0",
      "txTransfer": "0.0 B",
      "txError": "0",
      "txDropped": "0",
      "txOverruns": "0",
      "txCarrier": "0",
      "txCollisions": "0"
   },
   "usb0": {
      "rxBytes": "0",
      "rxTransfer": "0.0 B",
      "rxError": "0",
      "rxDropped": "0",
      "rxOverruns": "0",
      "rxFrame": "0",
      "txBytes": "0",
      "txTransfer": "0.0 B",
      "txError": "0",
      "txDropped": "0",
      "txOverruns": "0",
      "txCarrier": "0",
      "txCollisions": "0"
   }
}
```
